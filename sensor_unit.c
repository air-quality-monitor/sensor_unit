/*
 *  Combined sensor unit functionality
 */

#include <16F1827.h>
#DEVICE ADC=8                   // Select 8-bit mode

#fuses MCLR,INTRC_IO,NOWDT,NOPROTECT,NOBROWNOUT,NOPUT
//#fuses MCLR,INTRC_IO,WDT,NOPROTECT,NOBROWNOUT,NOPUT
#use delay(clock=8000000)
#use rs232(baud=38400, xmit=PIN_A1, stream=PC)

#include <stdint.h>
#include <stdbool.h>

#include "adxl345/adxl345.c"
#include "ds1307/ds1307.c"
#include "hpma115s0/hpma115s0.c"
#include "rgb_led/rgb_led.c"
#include "eeprom/eeprom.c"


// Global variables
int16_t accel_old;              // Store last changed accel value
uint8_t accel_stable;           // Number of tries since accel value changed


void setupDevices(void) {
  /*
   *  Set up accelerometer, RTC and particulate monitor
   */

  // Set up WDT to wake from sleep (but off for now)
  setup_wdt(WDT_32S);

  // Initialise accelerometer
  int data = 1;
  while (data != 0) data = init_adxl();

  restart_wdt();

  // Initialise RTC
  ds1307_init();

  // Initialise particulate monitor
  initPMSensor();
  startMeasurement();

  // Initialise Timer0 for LED patterns
  setupLEDTimer();

  // Initialise Timer1 to check accelerometer
  // Disabled because sleep was not working well enough
  //setup_timer_1(T1_INTERNAL | T1_DIV_BY_8);  // Every 0.26 seconds
  //enable_interrupts(INT_TIMER1);
}


Measurement takeMeasurement(void) {
  /*
   *  Take a particulate measurement and get the timestamp
   */

  uint16_t pm_data;             // Combined PM2.5 and PM10 measurements
  Measurement m;
  uint8_t discard;              // Variable to discard day of week

  ds1307_get_date(m.day, m.month, m.year, discard);
  ds1307_get_time(m.hour, m.minute, m.second);

  disable_interrupts(GLOBAL);   // Avoid interruptions with serial

  pm_data = measureParticles(); // Get data from sensor
  while (pm_data == 0xFF00) {   // Catch errors and retry
    fputs("Retry");
    pm_data = measureParticles();
  }

  m.PM10 = pm_data >> 8;        // Store PM10 and PM2.5 values
  m.PM2_5 = pm_data & 0xFF;

  enable_interrupts(GLOBAL);    // Re-enable interrupts to continue

  return m;
}


#INT_TIMER1
void checkAccelerometer(void) {
  /*
   *  Check accelerometer value and sleep if not moved
   */

  char xyz_data[6];             // Buffer for accelerometer data
  int16_t total;

  restart_wdt();

  read_XYZ(xyz_data);           // Read accelerometer value

  // Use sum of xyz since we're just looking for movement
  total = make16(xyz_data[1], xyz_data[0]) +
          make16(xyz_data[3], xyz_data[2]) +
          make16(xyz_data[5], xyz_data[4]);

  if (abs(total - accel_old) > 10) {  // Moved by more than noise threshold
    accel_stable = 0;                 // Reset stable count
    accel_old = total;                // Save accel value
  }
  else if (accel_stable > 16) {
    fputs("Sleep", PC);
    output_bit(HPM_ENABLE, 0);        // Switch off HPM
    setup_wdt(WDT_2S);                // Set WDT interval
    //sleep();                        // Sleep until WDT wakes PIC up (causing problems)
    delay_ms(2000);                   // PIC power usage minimal so just delay
    restart_wdt();
    fputs("Wake", PC);
  }
  else {                              // Not yet sleep time, but not moving
    accel_stable++;
    //fprintf(PC, "%u", accel_stable);
  }
}


void main() {
  setupDevices();

  resetEEPROM();

  Measurement m;

  for(int i=0; i<125; i++) {
    m = takeMeasurement();
    fprintf(PC, "\r\n%u-%u-%u %u:%u:%u %u %u",
            m.year, m.month, m.day, m.hour, m.minute, m.second,
            m.PM10, m.PM2_5);
    saveMeasurement(m);

    if (m.PM2_5 > 55) led_state = PM_HIGH;
    else if (m.PM2_5 > 40) led_state = PM_MEDIUM;
    else led_state = PM_LOW;

    delay_ms(100);
  }
}
